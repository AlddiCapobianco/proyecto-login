document.querySelector("#login_btn").setAttribute("onclick", "validacion()");
let user = document.querySelector("#username");
let pass = document.querySelector("#password");
let mens = document.querySelector("#mensaje");
user.focus();

fetch("usuarios.csv")
    .then(function (res) {
        return (res.text());
    })
    .then(function (data) {
        mostrarUsuarios(data);
    })

let usuarios_registrados = [];

function mostrarUsuarios(data) {
    let usuarios = data.split(/\r?\n|\r/);
    for (let i = 0; i < usuarios.length; i++) {
        let fila_usuario = usuarios[i].split(",");
        usuarios_registrados[i] = { nombre: fila_usuario[0], password: fila_usuario[1] };
    }
    console.log(usuarios_registrados);
}

function validacion() {
    mens.className = "rojo";
    if (user.value.trim() === "") {
        mens.value = "FALTA NOMBRE DE USUARIO";
        user.value = "";
        user.focus();
        return;
    }
    user.value = user.value.trim();

    if (pass.value.trim() === "") {
        mens.value = "FALTA PASSWORD";
        pass.value = "";
        pass.focus();
        return;
    }
    pass.value = pass.value.trim();

    let encontroUser;
    encontroUser = false;
    for (let i = 0; i < usuarios_registrados.length; i++) {
        if (usuarios_registrados[i].nombre === user.value && usuarios_registrados[i].password === pass.value) {
            encontroUser = true;
            break;
        }
    }
    if (encontroUser === false) {
        mens.value = "USUARIO INEXISTENTE";
        user.focus();
        user.value = "";
        pass.value = "";
        return;
    }

    mens.className = "verde";
    mens.value = "BIENVENIDO/A " + user.value.toUpperCase() + "...";
}

function KeyAscii(e) {
    return (document) ? e.keyCode : e.which;
}

function TabKey(e, nextobject) {
    nextobject = document.getElementById(nextobject);
    if (nextobject) {
        if (KeyAscii(e) == 13) nextobject.focus();
    }
}